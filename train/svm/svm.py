'''
SHOWS HOW TO TRAIN AN SVM USING THE SEQUENCE GENERATOR.
WHILE TRAINING AN SVM, THE SEQUENCE GENERATOR SHOULD RETURN ALL THE IMAGES INSIDE THE TRAIN SET AT ONE TIME.
SO THE BATCH SIZE IS SET TO BE EQUAL TO THE ENTIRE SIZE OF THE DATAFRAME.
WE ALSO DO NOT NEED ONE HOT ENCODING FOR SVM LABELS, AND THEY CAN BE SIMPLY THE SAME AS SPARSE.
SO WE OVERRIDE THE ONE_HOT_ENCODE FUNCTION AS WELL.
'''
import os
import sys
import shutil
#sys.path.insert(1, '/home/sme_admin/Downloads/Github/sequence_generator')
from pathlib import Path
fpath = Path(__file__).parent.absolute().__str__()
sys.path.insert(1, (fpath + '/../../../sequence_generator'))
print(sys.path)
from sequence_generator import *
from custom_metrics_and_losses import *
import numpy as np
from PIL import Image
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
import uuid
import pickle
#############################################################    
#from keras.models import Sequential
#from keras.layers import Conv2D, MaxPooling2D
#from keras.layers import Activation, Dropout, Flatten, Dense, GlobalAveragePooling2D
#from keras import backend as K
import keras
from skimage.filters import laplace, sobel, roberts

#from keras.applications import ResNet50
#from keras.applications.vgg16 import VGG16
#from keras.applications.resnet50 import preprocess_input as resnet_preprocess_input
#from keras.applications.vgg16 import preprocess_input as vgg16_preprocess_input
from sklearn import svm

from keras import Model, layers
from keras.models import load_model, model_from_json
#Add checkpoints 
from keras import backend as K

from keras.callbacks import ModelCheckpoint,CSVLogger
import tensorflow as tf
import albumentations as A
from imgaug import augmenters as iaa
from sklearn.metrics import classification_report, confusion_matrix



WEIGHTS_FILEPATH="./weights/weights.hdf5"
PRED_FILEPATH = "./pred/"

run_id = str(uuid.uuid4())
os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'
device_name = tf.test.gpu_device_name()
print("device name",device_name)
if device_name != '/device:GPU:0':
  raise SystemError('GPU device not found')
print('Found GPU at: {}'.format(device_name))
print("GPUs: ", len(tf.config.experimental.list_physical_devices('GPU')))



def get_model(weights=None):
    svm_model = svm.SVC(C=100,kernel='linear')
    return svm_model

## to predict a single image
def predict_image(filepath,model=None,mode=None):
    t1 = time.monotonic()
    image = None

    if mode is None:
        image = cv2.imread(filepath)
        image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    else:
        image = cv2.imread(filepath,mode)

    image = cv2.resize(image,(224,224))
    lap_feat = laplace(image)
    k = [[lap_feat.mean(),lap_feat.var(),np.amax(lap_feat)]]
    results = model.predict(k)
    t2 = time.monotonic()
    print("total time",(t2-t1))
    return results

def predict(test_gen,model):

    iterator = test_gen.__iter__()

    batch = iterator.__next__()

    X_test = batch[0]
    
    Y_test = batch[1]

    y_pred = model.predict(X_test)

    print("confusion matrix")
    print(confusion_matrix(Y_test,y_pred))
    print(classification_report(Y_test,y_pred,target_names=["blurred","clear"]))

    '''
    y_pred = get_model(WEIGHTS_FILEPATH).predict_generator(test_gen)
    
    y_pred = np.argmax(y_pred,axis=1)

    print("y true",test_gen.__string_labels_to_integer_labels__(test_gen.data_frame.category.values))
    print("confusion matrix")
    print(confusion_matrix(test_gen.__string_labels_to_integer_labels__(test_gen.data_frame.category.values),y_pred))
    print("CLASSIFICATION REPORT")
    print(classification_report(test_gen.__string_labels_to_integer_labels__(test_gen.data_frame.category.values),y_pred,target_names=["0","1","2","3"]))
    print("------- copying image results to path---------",PRED_FILEPATH)
    copy_classified_images(test_gen,y_pred)
    '''


def _train(train_gen,val_gen,model):

    iterator = train_gen.__iter__()

    batch = iterator.__next__()

    X_train = batch[0]
    
    Y_train = batch[1]

    #print(X_train.shape)
    #print(Y_train.shape)
    #print(Y_train)
    
    model.fit(X_train,Y_train)

    return model

    #history = get_model().fit_generator(train_gen,epochs=800,validation_data = val_gen,callbacks=get_callbacks())

    #pd.DataFrame(history.history).to_csv("./history/history.csv")


def write_seed(seed):
    f = open("./seeds/seeds.txt", "w")
    f.write(str(seed))
    f.close()


def save_model(model,filename="blur_detector.sav"):
    filename = './saved_models/' + filename
    return pickle.dump(model, open(filename, 'wb'))

def load_saved_model(filename="blur_detector.sav"):
    filename = './saved_models/' + filename
    return pickle.load(open(filename, 'rb'))


base_path = "/home/sme_admin/Downloads/Github/pupil_classification/lib/blur_detection/data/"

df = SequenceGenerator.build_df(
    {"clear" : {"image_paths" : [base_path + "clear/"], "image_indices" : None, "label_paths" : [], "meta_data_paths" : []}, "blurred" : {"image_paths" : [base_path + "blurred/"], "image_indices" : None, "label_paths" : [], "meta_data_paths" : []}})


random_seed = random.random()
random_seed = 0.6494467546111402

train,val,test = SequenceGenerator.split_df(df,random_seed)

## TRANSFORMATIONS ARE SET TO FALSE IN THIS.
transform = A.ReplayCompose([
        A.ShiftScaleRotate(p=0.9),
        A.Rotate(p=0.9),
        A.Blur(p=0.7),
        A.RandomGamma(p=0.9),
        A.Transpose(p=0.5),
        A.RandomFog(p=0.8,fog_coef_lower=0.8,fog_coef_upper=1.0,alpha_coef=0.8),
        A.RandomSunFlare(flare_roi=(0, 0, 1, 0.5), angle_lower=0.5, p=0.9),
        A.HueSaturationValue(p=0.8),
        A.OneOf([
            A.IAAAdditiveGaussianNoise(p=0.9),
            A.GaussNoise(p=0.6),
        ], p=0.5),
        A.OneOf([
            A.RandomBrightnessContrast(p=0.9),
            A.Downscale(p=0.9)
        ], p=0.9)
    ], p=0.5)




preprocessing_func = None

## OVERRIDE THE SUBCLASS, OR PASS IN THE FUNCTION, EITHER OPTION IS AVAILABLE.
class CustomSequenceGenerator(SequenceGenerator):

    def laplace_features(self,image):
        
        lap_feat = laplace(image)
        k =  [lap_feat.mean(),lap_feat.var(),np.amax(lap_feat)]
        #print(k)
        return k

    def __read_image__(self,image_path):
        return self.resize(cv2.imread(image_path,0))


    def __merge_uuid_image__(self,uuid_images,uuid_labels,uuid_meta_data):
        ## this is all we have to change, and the model itself.
        #k = np.array(uuid_images)
        #k = inceptionv3_preprocess_input(k)
        #uuid_labels = list(set(uuid_labels))
        #k = self.feature_extractor().predict(k)
        #[["cataract"],["cataract"],["catataract"]]

        k = [self.laplace_features(x) for x in uuid_images]
        return k,uuid_labels

## AVOID SPECIFICATION OF BATCH SIZE SO THAT IT TAKES THE ENTIRE DATA FRAME LENGTH AS THE BATCH SIZE.
total_gen = CustomSequenceGenerator(data_frame=df,image_dims=(224,224,3),name="total",preprocessing_func=preprocessing_func,one_hot=False)

train_gen = CustomSequenceGenerator(data_frame=train,image_dims=(224,224,3),augment=False,name="train",transform=transform,shuffle=True,preprocessing_func=preprocessing_func,one_hot=False)

train_gen.describe()

val_gen = CustomSequenceGenerator(data_frame=val,image_dims=(224,224,3),n_classes=train_gen.n_classes,augment=False,name="validation",preprocessing_func=preprocessing_func,one_hot=False)

val_gen.describe()

test_gen = CustomSequenceGenerator(data_frame=test,image_dims=(224,224,3),n_classes=train_gen.n_classes,augment=False,name="test",preprocessing_func=preprocessing_func,one_hot=False)

test_gen.describe()

write_seed(random_seed)

#model = _train(train_gen,val_gen,get_model())

#print(train_gen.data_frame)

#predict(val_gen,model)

#save_model(model)

model = load_saved_model()
results = predict_image("./test_images/clear/1.png",model)
print(results)

results = predict_image("./test_images/blurred/1.png",model,0)
print(results)

'''

'''