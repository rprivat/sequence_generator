import pandas as pd
import numpy as np
import tensorflow as tf
import keras
import time
import cv2
import glob
import random
import os
import re
import itertools
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.utils import to_categorical
from sklearn.utils.class_weight import compute_class_weight
from sklearn.utils import class_weight
from keras import backend as K
import albumentations as A

import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')


from imgaug import augmenters as iaa

'''
Sequence Generator that supports albumentations, as well as semantic segmenetation and internally handles one hot encoding. 
'''
class SequenceGenerator(tf.keras.utils.Sequence):

	## if the first entry in the dataframe, has label paths, that means its semantic segmenation.
	def is_semantic_segmentation(self):
		## if there are label paths.

		return self.data_frame.iloc[0].label_paths != []

	## numpy array of dimensions.
	## n(samples),(224,224,3)
	## before passing them in, make sure to replace the labels with their actual values.
	def get_class_weights(self,labels):
		return compute_class_weight('balanced', np.unique(np.ravel(labels,order='C')), np.ravel(labels,order='C'))


	## we can place them in a category.
	## in case of a semantic segmentation this cannot be done.
	## so here it has to actually check the label_paths, and if they exist, then it can set the classes.
	## for binary we don't need this.
	def set_n_classes(self):
		if self.is_semantic_segmentation() == True:
			random_items = random.choices(population=self.data_frame.index.values, k=500)
			print("max item",max(random_items),"total df length",self.data_frame.index.values[-1])
			#print(random_items)
			'''
			print(self.data_frame.iloc[0].label_paths[0])
			bare_label = cv2.imread(self.data_frame.iloc[0].label_paths[0])
			print("bare label size",bare_label.shape)
			fig,ax = plt.subplots(1,2)
			ax[0].imshow(bare_label)
			ax[1].imshow(cv2.resize(bare_label,(224,224)))
			plt.show()
			exit(1)
			'''
			####
			batch_images,batch_labels,batch_meta_data = self.__load_batch_images_and_labels__(random_items)
			self.n_classes = 0
			## from these same labels we can calculate it.
			## that's it.
			self.semantic_label_weights = self.get_class_weights(np.array([self.__semantic_label__(x) for x in self.__flatten_batch_labels__(batch_labels)]))
			self.n_classes = len(self.semantic_label_weights)
			
		else:
			self.n_classes = len(self.data_frame.category.unique())
		
	## so we can run it and see.
	def describe(self):
		print(self.name + " has ",len(self.data_frame.index.values),"samples, with",self.n_classes," categories", "semantic_label_weights", self.semantic_label_weights, "semantic label map",self.semantic_label_map)

	@staticmethod
	def get_image_uuid(path):
		basename = os.path.basename(path)
		
		uuid = re.sub(r'_\d+','',basename)
		return uuid


	## the meta data paths consist of any additional meta-data that may be loaded for each image.
	@staticmethod
	def build_uuid_dict_from_paths(args):
		uuid_dictionary = {}
		for category in args:
			for path_type in ["image_paths","label_paths","meta_data_paths"]:
				for i,image_dir in enumerate(args[category][path_type]):
					for ext in ["/*.png","/*.jpg"]:
						
						for image_path in glob.glob(image_dir + ext):

							uuid = SequenceGenerator.get_image_uuid(image_path)
							if uuid not in uuid_dictionary:
								uuid_dictionary[uuid] = {
									"category" : category,
									"image_indices" : args[category]["image_indices"],
									"image_paths" : [],
									"label_paths" : [],
									"meta_data_paths" : []
								}
							uuid_dictionary[uuid][path_type].append(image_path)
		return uuid_dictionary

	## all we need to do then is plug in the model into this generator
	## and we should be done, i can try all the video sequences etc.
	## since we can now albument them.
	## args should be like
	## {
	##   "category" => {"image_paths" : [], "label_paths": [], "image_indices" : []}
	## }
	@staticmethod
	def build_df(args):
		uuid_dictionary = SequenceGenerator.build_uuid_dict_from_paths(args)
		df_list = []
		for uuid in uuid_dictionary:
			df_list.append([uuid,uuid_dictionary[uuid]["image_paths"],uuid_dictionary[uuid]["label_paths"],uuid_dictionary[uuid]["category"],uuid_dictionary[uuid]["image_indices"],uuid_dictionary[uuid]["meta_data_paths"]])
		return pd.DataFrame(df_list,columns=["uuid","image_paths","label_paths","category","image_indices","meta_data_paths"])
		## now its done.

	## splits the original dataframe into three dataframes
	## train = 80
	## val = 10
	## test = 10
	## useful starting point for the generator.
	@staticmethod
	def split_df(df,random_seed):
		test=df.sample(frac=0.1,random_state=int(random_seed*100))
		train=df.drop(test.index)
		val = train.sample(frac=0.15,random_state=int(random_seed*100))
		train = train.drop(val.index)
		return train,val,test

	


	## @param[data_frame]:
	## COLUMSN : 
	## -- uuid(the bare uuid of the image)
	## -- image_paths(an array containing the paths to all the images of this uuid)
	## -- label_paths(array containing paths to labels for each image. can be left blank if binary classificaction is being done)
	## -- category[the image category, should be an integer, in case we are doing multi class classification, starting from 0]
	## -- image_indices[which images are to be considered from the image_paths], so if you want to take only some images from the image paths, then specify their indices here, if None it will take all.

	## @param[batch_size] : number of uuids, to consider per batch.
	## @param[image_dims][Tuple] : the final dimensions for each image, that will be fed to the cnn.
	## @param[n_classes] : how many classes are there, (for the to_categorical function)
	## @param[semantic_label_map] : a map of labels, of the structure:

	## @param[augment][True/False] : whether or not to augment the data.
	## @param[transform] : the albumentations transform object, can be speficied from outside.
	## @param[pre_processing_func] : if some special preprpcessing is to be done (for eg for resnet, it has its own preprocessing func.)
	def __init__(self, data_frame, batch_size=None,image_dims=(224,224,3),n_classes=None,semantic_label_map=None,augment=False,transform=None,preprocessing_func=None,name="data_frame",shuffle=False,semantic_label_weights=None,one_hot=True):
		self.data_frame = data_frame

		self.list_idx = data_frame.index.values
		print("total elements",len(self.list_idx))
		## if batch size is not speicifed, it is equal to the size of the data frame.
		if batch_size is None:
			self.batch_size = len(self.list_idx)
		else:
			self.batch_size = batch_size
		self.data_frame['images_length'] = self.data_frame.image_paths.map(len)
		self.feat_ext = None
		self.dim = np.array(image_dims)
		self.preprocessing_func = preprocessing_func
		self.augment = augment
		self.transform = transform
		self.semantic_label_weights = semantic_label_weights
		self.semantic_label_map = semantic_label_map
		if n_classes is None:
			self.set_n_classes()
		else:
			self.n_classes = n_classes
		self.name = name
		self.shuffle = shuffle
		self.one_hot = one_hot
		self.on_epoch_end()
	
	## if its a semantic segmentation problem.
	## and if its a categorical classification problem.

	def get_y_true(self):
		if self.is_semantic_segmentation():
			## load
			batch_images,batch_labels,batch_meta_dat = self.__load_batch_images_and_labels__(self.data_frame.index.values)
			labellized = np.array([self.__semantic_label__(x) for x in self.__flatten_batch_labels__(batch_labels)])
			return np.squeeze(labellized)
		else:
			required_images_length = len(self.data_frame.iloc[0].image_indices)
			#print("required images length",required_images_length)
			#print(self.data_frame)
			applicable_rows = self.data_frame.loc[self.data_frame['images_length'] >= required_images_length]
			#print("images length",applicable_rows['images_length'])
			#print("applicable rows total",len(applicable_rows))
			return self.__string_labels_to_integer_labels__(applicable_rows.category.values)			

	def __augment__semantic__(self,images,labels):
		first_transform = self.transform(image = images[0], mask=labels[0])
		images[0] = first_transform['image']
		labels[0] = first_transform['mask']
		for i in np.arange(1,len(images)):
			transform = A.ReplayCompose.replay(first_transform['replay'], image=images[i], mask=labels[i])
			images[i] = transform['image']
			labels[i] = transform['mask']
		return images,labels
	# so we have 1, 0.8, 0,5
	# subtract from mean -> 200,120,140
	# we simply return them.
	# so it resizes like this.
	# lets try it with 
	# we can try with what first.
	# binary classifier.
	def __augment__(self,images,labels=None):
		## currently does nothing, but can apply the transforms if the labels have the same dimensions as the images, otherwise not necessary.
		if self.transform is not None and self.augment == True:
			random.seed(random.random())
			if self.is_semantic_segmentation():
				return self.__augment__semantic__(images,labels)
			else:
				first_transform = self.transform(image = images[0])
				images[0] = first_transform['image']
				images[1:] = [A.ReplayCompose.replay(first_transform['replay'], image=x)['image'] for x in images[1:]]
		else:
			pass
			#print("not augmenting")
		random.seed()		
		return images,labels

	def resize(self,image):
		return cv2.resize(image,self.dim[:-1])

	def to_rgb(self,image):
		return cv2.cvtColor(image,cv2.COLOR_BGR2RGB)

	def img_read(self,image):
		return cv2.imread(image)

        
	def __len__(self):
		return int(np.ceil(float(len(self.data_frame)) / float(self.batch_size)))

	## LOADS META DATA FILES RELATED TO EACH IMAGE OF THE UUID IMAGES.
	def __load_uuid_images_meta_data__(self,uuid,dataframe_index):

		meta_data_files = []
		
		uuid_df = self.data_frame.loc[dataframe_index]
		
		image_paths = uuid_df['image_paths']

		meta_data_paths = uuid_df['meta_data_paths']
		#print("meta data paths",meta_data_paths)
		
		image_indices = uuid_df['image_indices']

		if image_indices is None:
			#print("image indices was none inside uuid meta data paths")
			image_indices = np.arange(len(image_paths))
		
		max_index = max(image_indices)

		if len(meta_data_paths) > max_index:
			for idx in image_indices:
				## if it ends in png or jpg. 
				## i.e it is an image.
				filename, file_extension = os.path.splitext(meta_data_paths[idx])
				if file_extension in [".png",".jpg"]:
					meta_data_files.append(self.resize(self.img_read(meta_data_paths[idx])))
				else:
					continue

				
		#print("uuid image shape",uuid_images[0].shape)
		return meta_data_files

	def __read_image__(self,image_path):
		self.to_rgb(self.resize(self.img_read(image_path)))

	## so the pupil segmentation, the binary classifier as well as the semantic segmenter -> have to possible to send through this.
	## somehow.
	## given a uuid, we may have to load multiple images for it.
	## so we load and return them here.
	## after this just get the binary classifier working with this.
	def __load_uuid_images__(self,uuid,dataframe_index):
		## given a uuid, which are the 
		uuid_images = []
		
		uuid_df = self.data_frame.loc[dataframe_index]
		
		image_paths = uuid_df['image_paths']
		
		image_indices = uuid_df['image_indices']

		if image_indices is None:
			image_indices = np.arange(len(image_paths))
		
		max_index = max(image_indices)

		if len(image_paths) > max_index:
			for idx in image_indices:
				uuid_images.append(self.__read_image__(image_paths[idx]))
		#print("uuid image shape",uuid_images[0].shape)
		return uuid_images

	## do the resize here itself, color mapping can be done elsehwere.
	def __load_uuid_labels__(self,uuid,dataframe_index):
		uuid_labels = []
		uuid_df = self.data_frame.loc[dataframe_index]
		label_paths = uuid_df['label_paths']
		image_paths = uuid_df['image_paths']
		#print("label paths",label_paths,"image paths",image_paths)
		image_indices = uuid_df['image_indices']
		category = uuid_df['category']

		if image_indices is None:
			image_indices = np.arange(len(image_paths))

		max_index = max(image_indices)		

		if len(label_paths) > 0:
			if len(label_paths) > max_index:
				for idx in image_indices:
					uuid_labels.append(self.resize(self.img_read(label_paths[idx])))
		else:
			uuid_labels.extend([category for x in image_indices])
		return uuid_labels

	## all comes down to this shit only.
	## for each index -> we 
	def __load_batch_images_and_labels__(self,dataframe_indices):
		batch_images,batch_labels,batch_meta_data = [],[],[]
		image_uuids = self.data_frame.loc[dataframe_indices].uuid.values
		for i,df_index in enumerate(dataframe_indices):
			uuid_images = self.__load_uuid_images__(image_uuids[i],df_index)
			uuid_labels = self.__load_uuid_labels__(image_uuids[i],df_index)
			uuid_images_meta_data = self.__load_uuid_images_meta_data__(image_uuids[i],df_index)
			if len(uuid_images) > 0:
				batch_images.append(uuid_images)
				batch_labels.append(uuid_labels)
				batch_meta_data.append(uuid_images_meta_data)
		return batch_images,batch_labels,batch_meta_data


	## if you want to mash the individual frames of the uuid, into some kind of dimensions then here is the place.
	## herw we can execute the generation of the actual feature vector.
	## it should return both compressed if required.
	def __merge_uuid_image__(self,uuid_images,uuid_labels,uuid_meta_data_files):
		return uuid_images,uuid_labels
		
	## we should now flatten the list of images.
	def __flatten_batch_images__(self,batch_images):
		return list(itertools.chain.from_iterable(batch_images))

	def __flatten_batch_labels__(self,batch_labels):
		return list(itertools.chain.from_iterable(batch_labels))


	def __semantic_label__(self,label_image):
		#label = np.zeros(label_image.shape[:-1],dtype=np.uint8)
		## i made the label zeros.
		## 
		#label = np.expand_dims(label,-1)
		
		#print("sematnicizing")
		#t1 = time.monotonic()
		black = np.all((label_image == (0,0,0)),axis=2)
		
		## so if channel 0 is maximum -> iris
		## if channel 1 is maximum -> reflection
		## if channel 2 is maximum -> pupil
		label = np.argmax(label_image,axis=2)
		label = np.expand_dims(label,-1)
		label = label + 1
		label[black] = 0
		#t2 = time.monotonic()
		#print("time on armax",t2-t1)
		
		
		#label_image[:,:,j] = 255
		
		#print(j.shape)

		#label_image[k == (0,0,0)] = 0
		
		#for i,channel in enumerate(self.semantic_label_map):
		#	label[label_image[:,:,channel] == self.semantic_label_map[channel]] = i + 1
		
		#fig,ax = plt.subplots(1,1)
		#ax.imshow(label*30)
		#plt.show()

		return label

	def __string_labels_to_integer_labels__(self,string_labels):
		#print("string labels",string_labels)
		code = np.array(string_labels)
		label_encoder = LabelEncoder()
		vec = label_encoder.fit_transform(code)
		return vec

	def __one_hot_labels__(self,batch_labels):
		vec = None
		if self.semantic_label_map is not None:
			batch_labels = [self.__semantic_label__(x) for x in batch_labels]
			vec = np.array(batch_labels)
		else:
			vec = self.__string_labels_to_integer_labels__(batch_labels)

		if self.one_hot is True:
			batch_labels = to_categorical(vec, num_classes=self.n_classes)
		else:
			batch_labels = vec

		return batch_labels

	def __reshape_labels__(self,batch_images,batch_labels):
		return batch_labels

	## @param[batch_images] : list of lists. each element in the outer list contains 'n' images for a given uuid.
	def __preprocess__(self,batch_images,batch_labels,batch_meta_data):
		## so we are checking video flows ->
		## as a recurrent
		for index in np.arange(len(batch_images)):
	
			## while augmenting the images, we need to pass the relevant batch labels if any exist -> for the uuids of this image itself.
			batch_images[index],batch_labels[index] = self.__augment__(batch_images[index],batch_labels[index])

			## when preprocessing the labels -> in case of to_categorical or in case of -> 
			## so in case of resnet this will subtract all the means as required.
			if self.preprocessing_func is not None:
				batch_images[index] = [self.preprocessing_func(x) for x in batch_images[index]]


			## we can override this function if required.
			## so at this stage.
			batch_images[index],batch_labels[index] = self.__merge_uuid_image__(batch_images[index],batch_labels[index],batch_meta_data[index])

			
		
		batch_images = [x for x in batch_images if len(x) > 0]
		batch_labels = [x for x in batch_labels if len(x) > 0]

		batch_images = self.__flatten_batch_images__(batch_images)
		batch_labels = self.__flatten_batch_labels__(batch_labels)
		batch_labels = self.__one_hot_labels__(batch_labels)

		#print("batch images",batch_images)
		#print("batch labels",batch_labels)
		batch_images = np.array(batch_images)
		batch_labels = self.__reshape_labels__(batch_images,np.array(batch_labels))
		## we have to reshape this.
		##print("batch images shape",batch_images.shape,"batch labels shape",batch_labels.shape)
		return batch_images,batch_labels


  
	def __getitem__(self, index):
		t1 = time.monotonic()
		batch_idx = self.indices[index*self.batch_size:(index+1)*self.batch_size]
		####INSIDE LOAD_BATCH_IMAGES_AND_LABELS WE use loc, so we would need the actual index values, for that first we do iloc.
		batch_idx = self.data_frame.iloc[batch_idx].index.values
		Data,Target,meta_data = self.__load_batch_images_and_labels__(batch_idx)
		Data,Target = self.__preprocess__(Data,Target,meta_data)
		return Data,Target
		
    
	def on_epoch_end(self):
		self.indices = np.arange(len(self.list_idx))
		if self.shuffle == True:
			np.random.shuffle(self.indices)