from keras import backend as K

class CustomMetricsAndLosses:

	@staticmethod
	def weighted_categorical_crossentropy(weights):
    
		weights = K.variable(weights)

		def loss(y_true, y_pred):
			# scale predictions so that the class probas of each sample sum to 1
			y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
			# clip to prevent NaN's and Inf's
			y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
			#print("y_true dtype",y_true.dtype,"y_pred type",y_pred.dtype,"weights dtype",weights.dtype)
			# calc
			#y_true = tf.cast(y_true,tf.float32)
			try:
				loss = y_true * K.log(y_pred) * weights
			except:
				print("y_true dtype",y_true.dtype,"y_pred type",y_pred.dtype,"weights dtype",weights.dtype)
			loss = -K.sum(loss, -1)
			return loss
    
		return loss


	@staticmethod
	def recall(y_true, y_pred):
		true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
		all_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
		recall = true_positives / (all_positives + K.epsilon())
		return recall

	@staticmethod
	def precision(y_true, y_pred):
		true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
		predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
		precision = true_positives / (predicted_positives + K.epsilon())
		return precision

	@staticmethod
	def f1_score(y_true, y_pred):
		prec = CustomMetricsAndLosses.precision(y_true, y_pred)
		rec = CustomMetricsAndLosses.recall(y_true, y_pred)
		return 2*((prec*rec)/(prec+rec+K.epsilon()))

	@staticmethod
	def jacard_coef(y_true, y_pred):
		y_true_f = K.flatten(y_true)
		y_pred_f = K.flatten(y_pred)
		intersection = K.sum(y_true_f * y_pred_f)
		return (intersection + 1.0) / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection + 1.0)