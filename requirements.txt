# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: linux-64
_libgcc_mutex=0.1=conda_forge
_openmp_mutex=4.5=1_gnu
_tflow_select=2.1.0=gpu
absl-py=0.14.1=pyhd8ed1ab_0
aiohttp=3.7.4.post0=py39h3811e60_0
albumentations=1.1.0=pypi_0
alsa-lib=1.2.3=h516909a_0
amqp=5.0.6=pyhd8ed1ab_0
appdirs=1.4.4=pyh9f0ad1d_0
astor=0.8.1=pyh9f0ad1d_0
astunparse=1.6.3=pyhd8ed1ab_0
async-timeout=3.0.1=py_1000
attrs=21.2.0=pyhd8ed1ab_0
backports=1.0=py_2
backports.functools_lru_cache=1.6.4=pyhd8ed1ab_0
billiard=3.6.4.0=py39h3811e60_0
blinker=1.4=py_1
blosc=1.21.0=h9c3ff4c_0
brotli=1.0.9=h7f98852_5
brotli-bin=1.0.9=h7f98852_5
brotlipy=0.7.0=py39h3811e60_1001
brunsli=0.1=h9c3ff4c_0
bzip2=1.0.8=h7f98852_4
c-ares=1.17.2=h7f98852_0
c-blosc2=2.0.4=h5f21a17_1
ca-certificates=2021.10.8=ha878542_0
cachetools=4.2.4=pyhd8ed1ab_0
cairo=1.16.0=h6cf1ce9_1008
celery=5.1.2=pyhd8ed1ab_0
certifi=2021.10.8=py39hf3d152e_0
cffi=1.14.6=py39h4bc2ebd_1
cfitsio=3.470=hb418390_7
chardet=4.0.0=py39hf3d152e_1
charls=2.2.0=h9c3ff4c_0
click=7.1.2=pyh9f0ad1d_0
click-didyoumean=0.0.3=pyh8c360ce_0
click-plugins=1.1.1=py_0
click-repl=0.2.0=pyhd8ed1ab_0
cloudpickle=2.0.0=pyhd8ed1ab_0
colorama=0.4.4=pyh9f0ad1d_0
cryptography=3.4.7=py39hbca0aa6_0
cudatoolkit=10.1.243=h036e899_9
cudnn=7.6.5.32=hc0a50b0_1
cupti=10.1.168=0
cycler=0.10.0=py_2
cython=0.29.24=py39he80948d_0
cytoolz=0.11.0=py39h3811e60_3
dask-core=2021.9.1=pyhd8ed1ab_0
dataclasses=0.8=pyhc8e2a94_3
dbus=1.13.6=h48d8840_2
decorator=5.1.0=pyhd8ed1ab_0
dnspython=2.1.0=pyhd8ed1ab_0
efficientnet=1.0.0=pypi_0
expat=2.4.1=h9c3ff4c_0
fast-ml=3.68=pypi_0
ffmpeg=4.3.2=hca11adc_0
fontconfig=2.13.1=hba837de_1005
freetype=2.10.4=h0708190_1
fsspec=2021.10.0=pyhd8ed1ab_0
gast=0.4.0=pyh9f0ad1d_0
gettext=0.19.8.1=h73d1719_1008
giflib=5.2.1=h36c2ea0_2
glib=2.68.4=h9c3ff4c_1
glib-tools=2.68.4=h9c3ff4c_1
gmp=6.2.1=h58526e2_0
gnutls=3.6.13=h85f3911_1
google-auth=1.35.0=pyh6c4a22f_0
google-auth-oauthlib=0.4.6=pyhd8ed1ab_0
google-pasta=0.2.0=pyh8c360ce_0
graphite2=1.3.13=h58526e2_1001
grpcio=1.38.1=py39hff7568b_0
gst-plugins-base=1.18.5=hf529b03_0
gstreamer=1.18.5=h76c114f_0
h5py=2.10.0=nompi_py39h98ba4bc_106
harfbuzz=2.9.1=h83ec7ef_1
hdf5=1.10.6=nompi_h6a2412b_1114
icu=68.1=h58526e2_0
idna=2.10=pyh9f0ad1d_0
image-classifiers=1.0.0=pypi_0
imagecodecs=2021.8.26=py39h44211f0_1
imagedataaugmentor=0.0.0=pypi_0
imageio=2.9.0=py_0
imgaug=0.4.0=pypi_0
importlib-metadata=4.8.1=py39hf3d152e_0
jasper=1.900.1=h07fcdf6_1006
joblib=1.1.0=pyhd8ed1ab_0
jpeg=9d=h36c2ea0_0
jxrlib=1.1=h7f98852_2
keras=2.4.3=hd3eb1b0_0
keras-applications=1.0.8=pypi_0
keras-base=2.4.3=pyhd3eb1b0_0
keras-preprocessing=1.1.2=pyhd8ed1ab_0
kiwisolver=1.3.2=py39h1a9c180_0
kombu=5.1.0=py39hf3d152e_0
krb5=1.19.2=hcc1bbae_2
lame=3.100=h7f98852_1001
lcms2=2.12=hddcbb42_0
ld_impl_linux-64=2.36.1=hea4e1c9_2
lerc=3.0=h9c3ff4c_0
libaec=1.0.6=h9c3ff4c_0
libblas=3.9.0=11_linux64_openblas
libbrotlicommon=1.0.9=h7f98852_5
libbrotlidec=1.0.9=h7f98852_5
libbrotlienc=1.0.9=h7f98852_5
libcblas=3.9.0=11_linux64_openblas
libclang=11.1.0=default_ha53f305_1
libcurl=7.79.1=h2574ce0_1
libdeflate=1.8=h7f98852_0
libedit=3.1.20191231=he28a2e2_2
libev=4.33=h516909a_1
libevent=2.1.10=h9b69904_4
libffi=3.4.2=h9c3ff4c_4
libgcc-ng=11.2.0=h1d223b6_10
libgfortran-ng=11.2.0=h69a702a_10
libgfortran5=11.2.0=h5c6108e_10
libglib=2.68.4=h174f98d_1
libgomp=11.2.0=h1d223b6_10
libiconv=1.16=h516909a_0
liblapack=3.9.0=11_linux64_openblas
liblapacke=3.9.0=11_linux64_openblas
libllvm11=11.1.0=hf817b99_2
libnghttp2=1.43.0=h812cca2_1
libogg=1.3.4=h7f98852_1
libopenblas=0.3.17=pthreads_h8fe5266_1
libopencv=4.5.3=py39h70bf20d_1
libopus=1.3.1=h7f98852_1
libpng=1.6.37=h21135ba_2
libpq=13.3=hd57d9b9_1
libprotobuf=3.16.0=h780b84a_0
libssh2=1.10.0=ha56f1ee_2
libstdcxx-ng=11.2.0=he4da1e4_10
libtiff=4.3.0=hf544144_0
libuuid=2.32.1=h7f98852_1000
libvorbis=1.3.7=h9c3ff4c_0
libwebp-base=1.2.1=h7f98852_0
libxcb=1.13=h7f98852_1003
libxkbcommon=1.0.3=he3ba5ed_0
libxml2=2.9.12=h72842e0_0
libzlib=1.2.11=h36c2ea0_1013
libzopfli=1.0.3=h9c3ff4c_0
locket=0.2.0=py_2
lz4-c=1.9.3=h9c3ff4c_1
mahotas=1.4.11=py39hde0f152_1
markdown=3.3.4=pyhd8ed1ab_0
matplotlib=3.4.3=py39hf3d152e_0
matplotlib-base=3.4.3=py39h2fa2bec_1
multidict=5.2.0=py39h3811e60_0
mysql-common=8.0.25=ha770c72_2
mysql-connector-python=8.0.25=py39h664ec74_0
mysql-libs=8.0.25=hfa10184_2
ncurses=6.2=h58526e2_4
nettle=3.6=he412f7d_0
networkx=2.5=py_0
nspr=4.30=h9c3ff4c_0
nss=3.69=hb5efdd6_1
numpy=1.21.2=py39hdbf815f_0
oauthlib=3.1.1=pyhd8ed1ab_0
olefile=0.46=pyh9f0ad1d_1
opencv=4.5.3=py39hf3d152e_1
opencv-python=4.5.3.56=pypi_0
opencv-python-headless=4.5.3.56=pypi_0
openh264=2.1.1=h780b84a_0
openjpeg=2.4.0=hb52868f_1
openssl=1.1.1l=h7f98852_0
opt_einsum=3.3.0=pyhd8ed1ab_1
packaging=21.0=pyhd8ed1ab_0
pandas=1.3.3=py39hde0f152_0
partd=1.2.0=pyhd8ed1ab_0
patchify=0.2.3=pypi_0
patsy=0.5.2=pyhd8ed1ab_0
pcre=8.45=h9c3ff4c_0
pillow=8.3.2=py39ha612740_0
pip=21.2.4=pyhd8ed1ab_0
pixman=0.40.0=h36c2ea0_0
pooch=1.5.1=pyhd8ed1ab_0
prompt-toolkit=3.0.20=pyha770c72_0
prompt_toolkit=3.0.20=hd8ed1ab_0
protobuf=3.16.0=py39he80948d_0
pthread-stubs=0.4=h36c2ea0_1001
py-opencv=4.5.3=py39hef51801_1
pyasn1=0.4.8=py_0
pyasn1-modules=0.2.7=py_0
pycocotools=2.0.2=py39hce5d2b2_2
pycparser=2.20=pyh9f0ad1d_2
pyjwt=2.2.0=pyhd8ed1ab_0
pyopenssl=21.0.0=pyhd8ed1ab_0
pyparsing=2.4.7=pyh9f0ad1d_0
pyqt=5.12.3=py39hf3d152e_7
pyqt-impl=5.12.3=py39h0fcd23e_7
pyqt5-sip=4.19.18=py39he80948d_7
pyqtchart=5.12=py39h0fcd23e_7
pyqtwebengine=5.12.1=py39h0fcd23e_7
pysocks=1.7.1=py39hf3d152e_3
python=3.9.7=hb7a2778_3_cpython
python-dateutil=2.8.2=pyhd8ed1ab_0
python-flatbuffers=2.0=pyhd8ed1ab_0
python_abi=3.9=2_cp39
pytz=2021.3=pyhd8ed1ab_0
pyu2f=0.1.5=pyhd8ed1ab_0
pywavelets=1.1.1=py39hce5d2b2_3
pyyaml=5.4.1=py39h3811e60_1
qt=5.12.9=hda022c4_4
qudida=0.0.4=pypi_0
readline=8.1=h46c0cb4_0
requests=2.25.1=pyhd3deb0d_0
requests-oauthlib=1.3.0=pyh9f0ad1d_0
rsa=4.7.2=pyh44b312d_0
scikit-image=0.18.3=py39hde0f152_0
scikit-learn=0.24.2=py39h7c5d8c9_1
scipy=1.7.1=py39hee8e79c_0
seaborn=0.11.2=hd8ed1ab_0
seaborn-base=0.11.2=pyhd8ed1ab_0
segmentation-models=1.0.1=pypi_0
setuptools=58.2.0=py39hf3d152e_0
shapely=1.7.1=pypi_0
six=1.16.0=pyh6c4a22f_0
snappy=1.1.8=he1b5a44_3
split-folders=0.4.3=pypi_0
sqlite=3.36.0=h9cd32fc_2
statsmodels=0.13.0=py39hce5d2b2_0
tensorboard=2.6.0=pyhd8ed1ab_1
tensorboard-data-server=0.6.0=py39h95dcef6_0
tensorboard-plugin-wit=1.8.0=pyh44b312d_0
tensorflow=2.4.1=gpu_py39h8236f22_0
tensorflow-base=2.4.1=gpu_py39h29c2da4_0
tensorflow-estimator=2.6.0=py39he80948d_0
tensorflow-gpu=2.4.1=h30adc30_0
termcolor=1.1.0=py_2
thop=0.0.31-2005241907=pypi_0
threadpoolctl=3.0.0=pyh8a188c0_0
tifffile=2021.8.30=pyhd8ed1ab_0
tk=8.6.11=h27826a3_1
toolz=0.11.1=py_0
torch=1.9.0=pypi_0
torchvision=0.10.0=pypi_0
tornado=6.1=py39h3811e60_1
tqdm=4.62.2=pyhd8ed1ab_0
typing-extensions=3.10.0.2=hd8ed1ab_0
typing_extensions=3.10.0.2=pyha770c72_0
tzdata=2021c=he74cb21_0
urllib3=1.26.7=pyhd8ed1ab_0
vine=5.0.0=pyhd8ed1ab_1
wcwidth=0.2.5=pyh9f0ad1d_2
werkzeug=2.0.1=pyhd8ed1ab_0
wheel=0.37.0=pyhd8ed1ab_1
wrapt=1.13.1=py39h3811e60_0
x264=1!161.3030=h7f98852_1
xorg-kbproto=1.0.7=h7f98852_1002
xorg-libice=1.0.10=h7f98852_0
xorg-libsm=1.2.3=hd9c2040_1000
xorg-libx11=1.7.2=h7f98852_0
xorg-libxau=1.0.9=h7f98852_0
xorg-libxdmcp=1.1.3=h7f98852_0
xorg-libxext=1.3.4=h7f98852_1
xorg-libxrender=0.9.10=h7f98852_1003
xorg-renderproto=0.11.1=h7f98852_1002
xorg-xextproto=7.3.0=h7f98852_1002
xorg-xproto=7.0.31=h7f98852_1007
xz=5.2.5=h516909a_1
yaml=0.2.5=h516909a_0
yarl=1.7.0=py39h3811e60_0
zfp=0.5.5=h9c3ff4c_7
zipp=3.6.0=pyhd8ed1ab_0
zlib=1.2.11=h36c2ea0_1013
zstd=1.5.0=ha95c52a_0
