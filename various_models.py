import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, MaxPooling3D,ConvLSTM2D,TimeDistributed,BatchNormalization
from keras.layers import Activation, Dropout, Flatten, Dense, GlobalAveragePooling2D,Input
import tensorflow as tf


class VariousModels:

	@staticmethod
	def build_convnet(shape=(224,224, 3)):

	    momentum = .9
	    model = keras.Sequential()
	    model.add(Conv2D(64, (3,3), input_shape=shape,
	        padding='same', activation='relu'))
	    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
	    model.add(BatchNormalization(momentum=momentum))
	    model.add(MaxPooling2D())
	    
	    model.add(Conv2D(128, (3,3), padding='same', activation='relu'))
	    model.add(Conv2D(128, (3,3), padding='same', activation='relu'))
	    model.add(BatchNormalization(momentum=momentum))
	    
	    model.add(MaxPooling2D())
	    
	    model.add(Conv2D(256, (3,3), padding='same', activation='relu'))
	    model.add(Conv2D(256, (3,3), padding='same', activation='relu'))
	    model.add(BatchNormalization(momentum=momentum))
	    
	    model.add(MaxPooling2D())
	    
	    model.add(Conv2D(512, (3,3), padding='same', activation='relu'))
	    model.add(Conv2D(512, (3,3), padding='same', activation='relu'))
	    model.add(BatchNormalization(momentum=momentum))
	    
	    model.add(GlobalAveragePooling2D())
	    return model


	@staticmethod
	def lstm_model(frames=4, categories=[0,1,2,3]):
	    
	    model = Sequential()
	    model.add(TimeDistributed(VariousModels.build_convnet(), input_shape=(frames,224,224,3)))
	    # here, you can also use GRU or LSTM
	    model.add(tf.keras.layers.GRU(64))
	    # and finally, we make a decision network
	    model.add(Dense(1024, activation='relu'))
	    model.add(Dropout(.5))
	    model.add(Dense(512, activation='relu'))
	    model.add(Dropout(.5))
	    model.add(Dense(128, activation='relu'))
	    model.add(Dropout(.5))
	    model.add(Dense(64, activation='relu'))
	    model.add(Dense(len(categories), activation='softmax'))

	    #outputs = [branch(second_Pooling, 'cat_{}'.format(category)) for category in categories]
	    
	    #seq = Model(inputs=trailer_input, outputs=outputs)
	    
	    return model